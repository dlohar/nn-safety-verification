### Related Work ###

***1. Synthesis of Fixed-Point Programs, Eva Darulova, Viktor Kuncak, Rupak Majumdar, Indranil Saha, EMSOFT 2013***


*** 2. Learning and Verification of Feedback Control Systems using Feedforward Neural Networks***, Souradeep Dutta,Susmit Jha, Sriram Sankaranarayanan, Ashish Tiwari, IFAC 2018 ***

** SUMMARY: ** The paper presents an approach to learn and formally verify feedback laws for data-driven models
of neural networks. Neural networks are emerging as powerful and general data-driven representations
for functions. This has led to their increased use in data-driven plant models and the representation of
feedback laws in control systems. However, it is hard to formally verify properties of such feedback
control systems. The proposed learning approach uses a receding horizon formulation that samples from
the initial states and disturbances to enforce properties such as reachability, safety and stability. Next,
the prpoposed verification approach uses an over-approximate reachability analysis over the system, supported by
range analysis for feedforward neural networks. It reports promising results obtained by applying the proposed 
techniques on several challenging nonlinear dynamical systems.

***3. Output Range Analysis for Deep Feedforward Neural Network, Souradeep Dutta, Susmit Jha2, Sriram Sankaranarayanan1 and Ashish Tiwari, NFM 2018***

**Abstract**: Given a neural network (NN) and a set of possible inputs to the network
described by polyhedral constraints, we aim to compute a safe over-approximation
of the set of possible output values. This operation is a fundamental primitive
enabling the formal analysis of neural networks that are extensively used in a
variety of machine learning tasks such as perception and control of autonomous
systems. Increasingly, they are deployed in high-assurance applications, leading
to a compelling use case for formal verification approaches. In this paper, we
present an efficient range estimation algorithm that iterates between an expensive
global combinatorial search using mixed-integer linear programming problems,
and a relatively inexpensive local optimization that repeatedly seeks a local optimum
of the function represented by the NN. We implement our approach and
compare it with Reluplex, a recently proposed solver for deep neural networks.
We demonstrate applications of our approach to computing flowpipes for neural
network-based feedback controllers. We show that the use of local search in conjunction
with mixed-integer linear programming solvers effectively reduces the
combinatorial search over possible combinations of active neurons in the network
by pruning away suboptimal nodes.

***4. Safety Verification of Cyber-Physical Systems with Reinforcement Learning Control, HOANG-DUNG TRAN, FEIYANG CAI, MANZANAS LOPEZ DIEGO, PATRICK MUSAU, TAYLOR T. JOHNSON, and XENOFON KOUTSOUKOS, EMSOFT 2019***

**Abstract:** This paper proposes a new forward reachability analysis approach to verify safety of cyber-physical systems
(CPS) with reinforcement learning controllers. The foundation of our approach lies on two efficient, exact and
over-approximate reachability algorithms for neural network control systems using star sets, which is an efficient
representation of polyhedra. Using these algorithms, we determine the initial conditions for which a
safety-critical system with a neural network controller is safe by incrementally searching a critical initial condition
where the safety of the system cannot be established. Our approach produces tight over-approximation
error and it is computationally efficient, which allows the application to practical CPS with learning enable
components (LECs). We implement our approach in NNV, a recent verification tool for neural networks and
neural network control systems, and evaluate its advantages and applicability by verifying safety of a practical
Advanced Emergency Braking System (AEBS) with a reinforcement learning (RL) controller trained using the
deep deterministic policy gradient (DDPG) method. The experimental results show that our new reachability
algorithms are much less conservative than existing polyhedra-based approaches.We successfully determine
the entire region of the initial conditions of the AEBS with the RL controller such that the safety of the system
is guaranteed, while a polyhedra-based approach cannot prove the safety properties of the system.

***5. Incremental Verification of Fixed-Point Implementations of Neural Networks, Luiz Sena, Erickson Alves, Iury Bessa, Eddie Filho, Lucas Cordeiro, ArXiv 2020***

**Abstract:** Implementations of artificial neural networks (ANNs) might lead to failures, which are hardly predicted in the
design phase since ANNs are highly parallel and their parameters are barely interpretable. Here, we develop
and evaluate a novel symbolic verification framework using incremental bounded model checking (BMC),
satisfiability modulo theories (SMT), and invariant inference, to obtain adversarial cases and validate coverage
methods in a multi-layer perceptron (MLP).We exploit incremental BMC based on interval analysis to compute
boundaries from a neuron’s input. Then, the latter are propagated to effectively find a neuron’s output since
it is the input of the next one. This paper describes the first bit-precise symbolic verification framework to
reason over actual implementations of ANNs in CUDA, based on invariant inference, therefore providing
further guarantees about finite-precision arithmetic and its rounding errors, which are routinely ignored
in the existing literature. We have implemented the proposed approach on top of the efficient SMT-based
bounded model checker (ESBMC), and its experimental results show that it can successfully verify safety
properties, in actual implementations of ANNs, and generate real adversarial cases in MLPs. Our approach
was able to verify and produce adversarial examples for 85.8% of 21 test cases considering different input
images, and 100% of the properties related to covering methods. Although our verification time is higher than
existing approaches, our methodology can consider fixed-point implementation aspects that are disregarded
by the state-of-the-art verification methodologies.

***6. Correct-by-Design Synthesis of Neural Network Controllers, W. van der Velden, Master Thesis, 2020***

***7. Compiling KB-Sized Machine Learning Models to Tiny IoT Devices, Sridhar Gopinath, Nikhil Ghanathe, Vivek Seshadri, Rahul Sharma, PLDI 2019***

** Abstract:** Recent advances in machine learning (ML) have produced KiloByte-size models that can directly run on constrained
IoT devices. This approach avoids expensive communication between IoT devices and the cloud, thereby enabling
energy-efficient real-time analytics. However, ML models are expressed typically in floating-point, and IoT hardware
typically does not support floating-point. Therefore, running these models on IoT devices requires simulating IEEE-754
floating-point using software, which is very inefficient. We present SeeDot, a domain-specific language to express ML inference algorithms and a compiler that compiles
SeeDot programs to fixed-point code that can efficiently run on constrained IoT devices. We propose 1) a novel compilation strategy that reduces the search space for some key
parameters used in the fixed-point code, and 2) new efficient implementations of expensive operations. SeeDot compiles
state-of-the-art KB-sized models to various microcontrollers and low-end FPGAs. We show that SeeDot outperforms
1) software emulation of floating-point (Arduino), 2) highbitwidth fixed-point (MATLAB), 3) post-training quantization (TensorFlow-Lite), and 4) floating- and fixed-point FPGA
implementations generated using high-level synthesis tools.

***8. Deep learning with limited numerical precision, Suyog Gupta, Ankur Agrawal,Kailash Gopalakrishnan, Pritish Narayanan,  ICML 2015***

**Abstact**: Training of large-scale deep neural networks is often constrained by the available computational resources. We study the effect of limited precision data representation and computation on neural network training. Within the context of low-precision fixed-point computations, we observe the rounding scheme to play a crucial role in determining the network's behavior during training. Our results show that deep networks can be trained using only 16-bit wide fixed-point number representation when using stochastic rounding, and incur little to no degradation in the classification accuracy. We also demonstrate an energy-efficient hardware accelerator that implements low-precision fixed-point arithmetic with stochastic rounding.

***9. Output Range Analysis for Feed-Forward Deep Neural Networks via Linear Programming, Zhiwu Xu, Shengchao Qin, Zhong Ming, IEEE Transactions on Reliability 2022***
- handles elu, sigmoid activation functions with linear constraints tightly using upper anf lower bounds with tangents.
- performs interval subdivision
- splits the network in multiple small networks.

***10. Automatic Verification of Finite Precision Implementations of Linear Controllers, Junkil Park, Miroslav Pajic, Oleg Sokolsky, Insup Lee, TACAS 2017***
Summary: Given an linear time invariant (LTI) model of a controller, a finite-precision implementation (step function) using floating-point arithmetic, and an approximate equivalent precision, verify if the implementation is equivalent to the initial LTI model from the input output perspective. They use symbolic execution techniques and floating-point error analysis techniques and extract a model from the finite-precision implementation that includes roundoff errors (both absolute and relative). Then they compare this extracted model with the original using approximate input output equivalence checking.

***11. Formal non-fragile verification of step response requirements for digital state-feedback control systems, Thiago Cavalcante, Iury Bessa, Eddie Filho, Lucas Cordeiro, Journal of Control, Automation and Electrical Systems 2020***
