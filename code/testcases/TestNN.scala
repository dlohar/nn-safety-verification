
import daisy.lang._
import Vector._

object TestNN {

  def nn1(x: Vector): Vector = {
    require(lowerBounds(x, List(-10, -5)) &&
      upperBounds(x, List(10, 5)))

    val weights1 = Matrix(List(
      List(1.2, 2.3),
      List(3.4, 4.5),
      List(4.5, 5.6)))

    val weights2 = Matrix(List(
      List(1.1, 2.2, 3.3),
      List(2.2, 3.3, 4.4),
      List(3.3, 4.4, 5.5)))

    val bias1 = Vector(List(1.0, 2.0, 3.0))
    val bias2 = Vector(List(0.5, 0.6, 0.7))

    val layer1 = relu(weights1 * x + bias1)
    val layer2 = relu(weights2 * layer1 + bias2)

    val layer3 = relu(weights1 * layer2 + bias1)
    val layer4 = relu(weights2 * layer3 + bias2)

    val layer5 = relu(weights1 * layer4 + bias1)
    val layer6 = relu(weights2 * layer5 + bias2)

    val layer7 = relu(weights1 * layer6 + bias1)
    val layer8 = relu(weights2 * layer7 + bias2)

    val layer9 = relu(weights1 * layer8 + bias1)
    val layer10 = relu(weights2 * layer9 + bias2)

    layer10

  } ensuring(layer10 >= 10.0 &&  layer10 <= 10)

}
