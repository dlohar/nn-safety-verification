#!/bin/bash --posix

# generate output directory
if [ ! -d uniform_precision_code_gen ]; then
  mkdir uniform_precision_code_gen
fi
output_folder="uniform_precision_code_gen"
input_file_path="testcases/"
function_name="nn1"

declare -a benchmarks=(
  "SmallNN" \
  "TestNN"
)

# make sure daisy is compiled
sbt compile
if [ ! -e daisy ]
then
  sbt script
fi

for file in "${benchmarks[@]}"
do
  echo "****************************** Benchmark:" ${file} "******************************"
  # to add a timeout: /usr/bin/time -f "%e" timeout 10m
  ./daisy --qnn --codegen --lang=C --apfixed --precision=Fixed32 ${input_file_path}/${file}.scala 
  # --codegenNNOriginal

  # output gets generated in output/ folder with name objectName.cpp
  mv output/${file}.cpp ${output_folder}/

  vivado_hls scripts/qnn/vivado_compile_general.tcl ${file} ${function_name} ${output_folder}/${file}.cpp

  # the report is generated in folder object_name/solution/syn/report/${function_name}_csynth.rpt
  mv ${file}/solution/syn/report/${function_name}_csynth.rpt ${output_folder}/${file}_csynth.rpt
done









