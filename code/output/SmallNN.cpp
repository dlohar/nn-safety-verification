#include <hls_math.h>
#include <ap_fixed.h>


/* @pre: ((-10.0 <= x_0) && (x_0 <= 10.0) && (-5.0 <= x_1) && (x_1 <= 5.0)) */
/* @post: (res) => (res +/- 1.0e-05) */
void nn1(ap_fixed<32,5> x_0, ap_fixed<32,4> x_1, ap_fixed<32,2> _result[1]) {
  ap_fixed<32,1> weights1_0_0 = 0.1;
  ap_fixed<32,1> weights1_0_1 = 0.2;
  ap_fixed<32,1> weights1_1_0 = 0.2;
  ap_fixed<32,1> weights1_1_1 = 0.15;
  ap_fixed<32,1> weights2_0_0 = 0.1;
  ap_fixed<32,1> weights2_0_1 = 0.2;
  ap_fixed<32,2> bias1_0 = 1.0;
  ap_fixed<32,3> bias1_1 = 2.0;
  ap_fixed<32,1> bias2_0 = 0.5;
  ap_fixed<32,2> _tmp = (weights1_0_0 * x_0);
  ap_fixed<32,2> _tmp1 = (weights1_0_1 * x_1);
  ap_fixed<32,3> layer1_dot_0 = (_tmp + _tmp1);
  ap_fixed<32,3> _tmp2 = (weights1_1_0 * x_0);
  ap_fixed<32,1> _tmp3 = (weights1_1_1 * x_1);
  ap_fixed<32,3> layer1_dot_1 = (_tmp2 + _tmp3);
  ap_fixed<32,3> layer1_bias_0 = (layer1_dot_0 + bias1_0);
  ap_fixed<32,4> layer1_bias_1 = (layer1_dot_1 + bias1_1);
  ap_fixed<32,3> layer1_0 = fmax(0, layer1_bias_0);
  ap_fixed<32,4> layer1_1 = fmax(0, layer1_bias_1);
  ap_fixed<32,1> _tmp4 = (weights2_0_0 * layer1_0);
  ap_fixed<32,1> _tmp5 = (weights2_0_1 * layer1_1);
  ap_fixed<32,2> layer2_dot_0 = (_tmp4 + _tmp5);
  ap_fixed<32,2> layer2_bias_0 = (layer2_dot_0 + bias2_0);
  ap_fixed<32,2> layer2_0 = fmax(0, layer2_bias_0);
  _result[0] = layer2_0;
  
} // [0.5, 1.75] +/- 1.2130476551670474e-08

