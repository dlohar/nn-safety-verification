## 2 proposals ##
#### Precision-Aware Safety Verification of NN controllers ####
-   **Initial Idea:** Given a specific (uniform) precision verify if the safety property holds after a few timestamps. If it does, generate an implementation that can be directly run on FPGA/Xilinx
-   **Solution Idea:** Use daisy's range + error analysis to compute the ranges after the iterations.
-   **Advantages:** Sound over-approximation of the safe ranges considering the implementation errors
-   **Disadvantages:** slow(?), high over-approximation(?), not scalable(?)
-   **Idea1:** Using CBMC to find counterexamples? Can we solve the problem itself with CBMC?

#### Sound Fixed-Point Mixed-Precision Implementations for Safe NN Controllers #### 
-   **Initial Idea:** Given an error bound in the output, optmize fixed mixed precision such that the implementation satisfies both the safety property and the error bound. Finally, generate an implementation that can be directly run on FPGA/Xilinx.
-   **Solution Idea:** Use Aster's analysis + another constraint for safety property.
-   **Advantages:** Sound fast over-approximation considering implementation errors
-   **Disadvantages:** high over-approximation(?), not scalable(?)

